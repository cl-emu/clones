(in-package :cl-user)

(defpackage clones
  (:use :cl)
  (:import-from :clones.cpu
                :make-cpu
                :cpu-memory
                :reset)
  (:import-from :clones.ppu
                :ppu-dma-result
                :ppu-nmi-result
                :ppu-new-frame
                :sync)
  (:import-from :clones.input
                :handle-input)
  (:import-from :clones.instructions
                :single-step)
  (:import-from :clones.memory
                :memory-ppu
                :memory-gamepad
                :swap-rom)
  (:import-from :clones.util
                :slot->))

(in-package :clones)

(defvar *nes* (make-cpu))
(defvar *trace* nil)

(defun change-game (rom-file)
  ;; TODO: Reset other CPU+PPU slots, RAM, etc as necessary.
  (swap-rom (cpu-memory *nes*) rom-file)
  (reset *nes*))

(defun quit ()
  (format t "Thanks for playing!~%~%")
  #+sbcl (sb-ext:quit)
  #+ccl (ccl:quit))

(defun step-frame (nes &optional (headless nil))
  (with-accessors ((ppu memory-ppu)
                   (gamepad memory-gamepad)) (cpu-memory nes)
    (loop
      (let ((cycle-count (single-step nes)))
        (when *trace*
          (print nes)
          (clones.disassembler:now nes))
        (sync ppu (* cycle-count 3))
        (when (ppu-dma-result ppu)
          (clones.cpu:dma nes))
        (when (ppu-nmi-result ppu)
          (clones.cpu:nmi nes))
        (when (ppu-new-frame ppu)
          (when headless (return nil))
          (let ((input (handle-input gamepad)))
            (when (eq :quit input) (return nil))
            (clones.display:display-frame)))))))

(defun step-frames (frame-count)
  (dotimes (i frame-count)
    (step-frame *nes* t)))

(defun play ()
  (sdl2:init :everything)
  (clones.display:init-display)
  (loop for result = (step-frame *nes*)
        until (null result))
  (sdl2:quit))
